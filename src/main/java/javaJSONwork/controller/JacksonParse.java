package javaJSONwork.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.reflect.TypeToken;
import javaJSONwork.model.Craft;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class JacksonParse {
  public static void main(String[] args) {
    Type craftListType = new TypeToken<List<Craft>>() {}.getType();
    /*try {*/
    String filepath = ("E:\\java-offline-task12_JSON"
      + "\\src\\main\\resources\\packJSON\\craftsNew2.json");
    List<Craft> listCrafts = new ArrayList<>();
    listCrafts.add(new Craft("Storm Eagle", "Orbital Dropship", 1,
      "Vengeance launcher", "twin-linked Heavy Bolters",
      "2x combination rocket/afterburning turbofans", 2000,
      30000, 68.0, 18.0, 11.8, 7.2, "Unknown"));
    listCrafts.add(new Craft("Thunderhawk Gunship", "Orbital Dropship", 4,
      "Dorsal mounted Battle Cannon", "4x twin-linked Heavy Bolters",
      "3 x RX-92-00 Combination Rocket/Afterburning Turbofans", 2000, 28000,
      121.0, 26.6, 26.65, 9.8, "Unknown" ));
    listCrafts.add(new Craft("Valkyrie", "Airborne Assault Carrier", 4,
      "Hull mounted Multi-laser", "2x Hellstrike Missiles",
      "2x F75-MV Afterburning Vector-Turbojets", 1100, 2000,
      31.0, 18.5, 16.9, 4.8, "Mars"));
    ObjectMapper mapper = new ObjectMapper();
    try {
      mapper.writeValue(new File(filepath), listCrafts);
    } catch (IOException e) {
      e.printStackTrace();
    }
    try {
      List<Craft> craftR = mapper.readValue(new File(filepath), List.class);
      System.out.println(craftR);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
