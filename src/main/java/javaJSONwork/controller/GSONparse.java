package javaJSONwork.controller;

import com.google.gson.Gson;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.*;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import javaJSONwork.model.Craft;

public class GSONparse {
  private static ArrayList<Craft> crafts = new ArrayList<>();
  public static void main(String[] args) {
    Gson gson = new Gson();
    Type itemsListType = new TypeToken<List<Craft>>() {}.getType();
    /*try {*/
    String filepath = ("E:\\java-offline-task12_JSON"
      + "\\src\\main\\resources\\packJSON\\craftsNew1.json");
    List<Craft> listCrafts = new ArrayList<>();
    listCrafts.add(new Craft("Storm Eagle", "Orbital Dropship", 1,
      "Vengeance launcher", "twin-linked Heavy Bolters",
      "2x combination rocket/afterburning turbofans", 2000,
      30000, 68.0, 18.0, 11.8, 7.2, "Unknown"));
    listCrafts.add(new Craft("Thunderhawk Gunship", "Orbital Dropship", 4,
      "Dorsal mounted Battle Cannon", "4x twin-linked Heavy Bolters",
      "3 x RX-92-00 Combination Rocket/Afterburning Turbofans", 2000, 28000,
      121.0, 26.6, 26.65, 9.8, "Unknown" ));
    listCrafts.add(new Craft("Valkyrie", "Airborne Assault Carrier", 4,
      "Hull mounted Multi-laser", "2x Hellstrike Missiles",
      "2x F75-MV Afterburning Vector-Turbojets", 1100, 2000,
      31.0, 18.5, 16.9, 4.8, "Mars"));
    String jsonStr = new Gson().toJson(listCrafts);
    try (
      Writer writer = new FileWriter(filepath)) {
      Gson gsonB = new GsonBuilder().create();
      gsonB.toJson(listCrafts, writer);
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println(jsonStr);
    List<Craft> listItemsDes = new Gson().fromJson(jsonStr, itemsListType);
    System.out.println(listItemsDes.toString());
  }
}
